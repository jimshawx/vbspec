# vbSpec by Chris Cowley and Miklos Muhi

Chris Cowley's original web page can be found at http://freestuff.grok.co.uk/vbspec/

"They said it couldn't be done! (well actually, they only said it shouldn't be done, but let's not split hairs) - a Sinclair ZX Spectrum emulator, written entirely in Visual Basic!"

The project was taken over by Miklos Muhi and many improvements were made before the project was once again abandoned. That source can be found at https://sourceforge.net/projects/vbspec/.

The source is released under GPL V2.

